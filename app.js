
/**
 * Module dependencies.
 */

var express = require('express')
  , http = require('http');

var path = require('path');
var cookieParser = require('cookie-parser');
var fs = require('fs');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(cookieParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.get('/node_modules/angular/angular.js', function(req, res) {
    res.sendfile(path.join(__dirname, 'node_modules/angular', 'angular.js'));
});

app.get('/node_modules/angular-route/angular-route.js', function(req, res) {
    res.sendfile(path.join(__dirname, 'node_modules/angular-route', 'angular-route.js'));
});

app.get('/node_modules/angular-ui-router/release/angular-ui-router.js', function(req, res) {
    res.sendfile(path.join(__dirname, 'node_modules/angular-ui-router/release', 'angular-ui-router.js'));
});

// route for Angular SPA
app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});



// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
