/*
 * Main Angular module for app,
 * uses ui.router,uiGmapgoogle-maps,ngGeolocation 
 * as dependencies.
 */
var main =  angular.module('main',['ui.router','uiGmapgoogle-maps','ngGeolocation']);

    // ui-router route's configuration
    main.config(['$stateProvider', '$urlRouterProvider',
      function($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise("/");

        // Now set up the states
        $stateProvider
          .state('home', {
            url: "/",
            templateUrl: "./home/home.view.html",
            controller: 'homeCtrl'
          })
          .state('location', {
            url: "/location/:id",
            templateUrl: "./location/location.view.html",
            controller: 'locationCtrl'
        })
      }]);
    // custom directive for location card
    var loc = function() {
      return {
        restrict: 'E',
        scope: {
          item: '=item'
        },
        templateUrl: './home/loc.html'
      };
    };
    
    // wiring up controller,directive with "main" module and configuration of uiGmapgoogle-maps
    main
    .controller('locCtrl',function($scope){
    
    })
    .directive('loc',loc);
    main
    .config(function(uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyDVkresqu5NhmgmVZImr9E5n_6Yiadw9BY',
            v: '2.3.3', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });
    });