/*
 * Angular service to use foursquare API
 */
var main =  angular.module('main');

   var foursquare = function($http) {
       // method to get list of locations by map co-ords
        this.getLocations = function(map){
            return $http.get('https://api.foursquare.com/v2/venues/explore?client_id=HJYAW4G5FILENZNLJYTKLKJ5MOCVYD51ILNDVWKM0TPZ3ZB2&client_secret=YH3THYRMZQO55P2LDPVEXAMZ2UEYXDA10VFS0YV2AIFTM3AJ&v=20130815&ll='+ map.center.latitude +','+ map.center.longitude);
        }
        // method to get info of a location by its id
        this.getLocation = function(id){
            return $http.get('https://api.foursquare.com/v2/venues/'+ id +'?client_id=HJYAW4G5FILENZNLJYTKLKJ5MOCVYD51ILNDVWKM0TPZ3ZB2&client_secret=YH3THYRMZQO55P2LDPVEXAMZ2UEYXDA10VFS0YV2AIFTM3AJ&v=20130815');
        }
    }
 //wiring up of service with "main" module and injection in case of minification    
foursquare.$inject = ['$http'];
main.service('foursquare',foursquare);