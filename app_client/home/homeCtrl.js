/*
 * Angular contoller for Home page
 */
main =  angular.module('main');

    // controller function unsing foursquare service and geolocation
   var homeCtrl = function($scope,foursquare,$geolocation) {
            // Default Maps co-ords
            $scope.map = { center: { latitude: 30.693743, longitude: 76.842774 }, zoom:12};
            
            // use Promise of geolocation to get user's position co-ords
          $geolocation.getCurrentPosition({
                timeout: 60000
             }).then(function(position) {
                 // Promise successful
                 // set position co-ords on scope to "map" variable
                $scope.map = { center: { latitude: position.coords.latitude, longitude: position.coords.longitude }, zoom: 12 };
                var map = $scope.map;
        
            // use foursquare service to get list of locations by map co-ords
            foursquare.getLocations(map)
                .success(function (data) {
                    // set data on scope
                    $scope.data = data.response.groups['0'].items;
                })
                .error(function (e) {
                    $scope.data = e;
                });
             });
          

    }
    
    // wiring up controller to "main" module and injection for minification
    homeCtrl.$inject = ['$scope','foursquare','$geolocation'];
    main.controller('homeCtrl',homeCtrl)